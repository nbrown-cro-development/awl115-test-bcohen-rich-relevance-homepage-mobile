# 1. Original Form Action Configuration
This file outlines all the settings within the Monetate action using the below format.
```bash
Option	|	Value
```
For example
```bash
Relative Element Selector				|	#siteFooter
```
## Required Inputs
```bash
HTML String								|	*Empty*
Relative Element Selector				|	#r-siteFooter
Insert Method							|	Last Child
```

## Optional Inputs
```bash
Banner Div Inline Style					|	*Empty*
JavaScript								|	Copy & Paste contents of variation.js
CSS										|	Copy & Paste contents of variation.css
Select multiple elements if matched?	|	No
Re-check for Elements					|	No
```

## Targeting
```bash
Page Type Equals						|	main
```
